import React from 'react';
import ReactDOM from 'react-dom';

import { Route, BrowserRouter } from 'react-router-dom';

import Home from './components/views/home';
import About from './components/views/about';
import Process from './components/views/process';
import Clients from './components/views/clients';
import Contact from './components/views/contact';
import Thanks from './components/views/thanks';

import styles from "./stylesheets/style.css";
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <BrowserRouter basename={process.env.PUBLIC_URL}>
        <div>
            <Route exact path='/' component={Home} />
            <Route path='/about' component={About} />
            <Route path='/process' component={Process} />
            <Route path='/clients' component={Clients} />
            <Route path='/contact' component={Contact} />
            <Route path='/thanks' component={Thanks} />
        </div>
    </BrowserRouter>,
    document.getElementById('root')
);
registerServiceWorker();
