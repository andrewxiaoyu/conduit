import React, { Component } from 'react';
import Navigation from '../comp/nav';
import Footer from '../comp/footer';
import PricingPanels from '../comp/ppanels';
import ProcessPanels from '../comp/pprocess';

import '../../stylesheets/style.css';

class Process extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
      <div className="home">
        <Navigation page="process"></Navigation>
        <div className="content">
          <h1>Our Process</h1>
          <ProcessPanels></ProcessPanels>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default Process;
