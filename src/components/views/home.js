import React, { Component } from 'react';
import Landing from '../comp/landing';
import Navigation from '../comp/nav';
import Initiative from '../comp/initiative';
import Footer from '../comp/footer';

import '../../stylesheets/style.css';

class Home extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
      <div className="home">
        <Navigation page="home"></Navigation>
        <div className="content">
          <Landing></Landing>
          <Initiative></Initiative>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default Home;
