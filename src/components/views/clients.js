import React, { Component } from 'react';
import Navigation from '../comp/nav';
import Footer from '../comp/footer';
import ClientPanels from '../comp/cpanels';

class Client extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
        <div className="container">
            <Navigation page="clients"></Navigation>
            <div className="content">
              <h1>Past Clients</h1>
              <ClientPanels></ClientPanels>
            </div>
            <Footer></Footer>
        </div>
    );
  }
}

export default Client;
