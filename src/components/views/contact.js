import React, { Component } from 'react';
import Navigation from '../comp/nav';
import Footer from '../comp/footer';
import ContactOptions from '../comp/coptions';

import '../../stylesheets/style.css';

class Contact extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
      <div className="container">
        <Navigation page="contact"></Navigation>
        <div className="content">
          <h1>Contact Us</h1>
          <ContactOptions></ContactOptions>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default Contact;
