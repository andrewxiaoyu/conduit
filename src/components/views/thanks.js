import React, { Component } from 'react';
import Navigation from '../comp/nav';
import Footer from '../comp/footer';

import '../../stylesheets/style.css';
import '../../stylesheets/components/thanks.css';

class Thanks extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
        <div className="container">
            <Navigation page="thanks"></Navigation>
            <div className="content">
                <div id="thanks">
                    <img src={require("../../img/thankyou.png")} />
                    <h1>Thank You!</h1>
                    <p className="blue">We'll get back to you as soon as possible!</p>
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
  }
}

export default Thanks;
