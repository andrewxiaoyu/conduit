import React, { Component } from 'react';
import Navigation from '../comp/nav';
import Footer from '../comp/footer';
import Team from '../comp/team';
import AboutInfo from '../comp/aboutinfo';

class About extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render() {
    return (
        <div className="container">
            <Navigation page="about"></Navigation>
            <div className="content">
              <h1>About</h1>
              <AboutInfo></AboutInfo>
              <Team></Team>
            </div>
            <Footer></Footer>
        </div>
    );
  }
}

export default About;
