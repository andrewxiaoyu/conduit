import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../../font/stylesheet.css';
import '../../stylesheets/components/landing.css';

class Landing extends Component {
  constructor (props) {
    super(props)
  }
  componentDidMount() {
  }
  componentWillUnmount () {
  }
  tick () {
    // this.setState({count:(this.state.count+1), text:(devText[this.state.count%devText.length])});
  }
  render() {
    return (
      <div className="landing">
        <h1>Welcome to the <span className="blue">Conduit</span></h1>
        <h2>Connecting Businesses to Student Developers </h2>
        <Link to="/contact">
          <button>I Need A Website/App</button>
        </Link>
      </div>
    );
  }
}

export default Landing;
