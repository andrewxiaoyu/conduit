import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Join extends Component {
  render() {
    return (
      <div className="join">
          <h2>Join the Team</h2>
          <form acceptCharset="UTF-8" action="https://formkeep.com/f/5e80fc15b9a7" method="POST">
            <div className="inputcont">
              <h4>Name</h4>
              <input type="text" name="name" placeholder="John Smith" required/>
            </div>
            <div className="inputcont">
              <h4>Email</h4>
              <input type="email" name="email" placeholder="johnsmith@email.com" required/>
            </div>
            <input type="submit"/>
          </form>
      </div>
    );
  }
}

export default Join;
