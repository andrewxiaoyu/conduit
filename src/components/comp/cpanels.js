import React, { Component } from 'react';

import '../../stylesheets/components/client.css';

const clientList = [
    {
        img:require("../../img/vivensity.png"),
        key:"Vivensity",
        link:"http://vivensity.com",
        desc:"Health, Wellness and Fitness Website",
    }
]

class ClientPanels extends Component {
    constructor(props){
        super(props);
        this.listItems = clientList.map((team) =>
            <li className="client" key={team.key}>
                <img src={team.img}/>
                <a target="_blank" href={team.link}>
                    <h4>{team.key}</h4>
                </a>
                <p>{team.desc}</p>
            </li>
        );
    }
    render() {
        return (
            <div className="clientPanels">
                <ul className="clients">{this.listItems}</ul>
            </div>
        );
    }
}

export default ClientPanels;
