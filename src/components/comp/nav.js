import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../../stylesheets/components/navigation.css';

class Navigation extends Component {
  openMenu(){
    document.getElementById("links").style.display="flex";
    document.getElementById("menu").style.display="none";
  }
  closeMenu(){
    document.getElementById("links").style.display="none";
    document.getElementById("menu").style.display="inline-block";
  }
  render() {
    console.log(this.props.page);
    let home = <h4>Home</h4>;
    let about = <h4>About</h4>
    let process = <h4>Process</h4>
    let clients = <h4>Clients</h4>
    let contact = <h4>Contact</h4>
    if(this.props.page === "home"){
      home = <h4 className="blue">Home</h4>;
    }
    if(this.props.page === "about"){
      about = <h4 className="blue">About</h4>;
    }
    if(this.props.page === "process"){
      process = <h4 className="blue">Process</h4>;
    }
    if(this.props.page === "clients"){
      clients = <h4 className="blue">Clients</h4>;
    }
    if(this.props.page === "contact"){
      contact = <h4 className="blue">Contact</h4>;
    }
    return (
      <div className="navigation">
        <h4 id="menu" onClick={this.openMenu.bind(this)}>&#9776;</h4>
        <div id="links">
          <Link to="/">{home}</Link>
          <Link to="/about">{about}</Link>
          <Link to="/process">{process}</Link>
          <Link to="/clients">{clients}</Link>
          <Link to="/contact">{contact}</Link>
          <h4 id="close" onClick={this.closeMenu.bind(this)}>&times;</h4>
        </div>
      </div>
    );
  }
}

export default Navigation;
