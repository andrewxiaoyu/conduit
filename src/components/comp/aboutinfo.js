import React, { Component } from 'react';

import '../../stylesheets/components/aboutinfo.css';

class Team extends Component {

  render() {
    return (
        <div className="aboutinfo">
            <h2>The <span className="blue">Conduit</span> Initiative</h2>
            <p>For the past few years, we have seen a push for computer science education in America. Everyone, especially teenagers currently in high school, are encouraged to learn how to code - and for good reason. The projected average annual job openings for computer science far outpace annual degrees earned in the field. 52% of all STEM job openings from 2010-2020 will be in the field of computing and mathematics.</p>
            <p>We at The Conduit Initiative program aim to change all of that. We want students to learn to code - and then apply those skills. Furthermore, in a world where eager startups eye billion-dollar acquisitions and increasing profits, we want to introduce technology as a lever to create positive impacts on the communities in which students live.</p>
        </div>
    );
  }
}

export default Team;
