import React, { Component } from 'react';

import '../../stylesheets/components/team.css';

const teamList = [
    // {
    //     key:"Sachin Jain",
    //     img:require("../../img/sachinjain.jpg"),
    //     desc: "Sachin Jain is a senior at Thomas Jefferson High School for Science and Technology who loves hackathons, startups, and presenting!"
    // },
    {
        key:"Andrew Wang",
        img:require("../../img/andrewwang.jpg"),
        desc: "Andrew Wang is a jack of all trades, with experience in web and mobile development, algorithmic coding, and graphic design."
    }
]

class Team extends Component {
    constructor(props){
        super(props);
        this.listItems = teamList.map((team) =>
            <li className="teamImg" key={team.key}>
                <img className="photo" src={team.img}></img>
                <h4>{team.key}</h4>
                <p>{team.desc}</p>
            </li>
        );
    }
    render() {
        return (
            <div className="team">
                <h2>Our Team</h2>
                <ul className="teamImgs">{this.listItems}</ul>
            </div>
        );
    }
}

export default Team;
