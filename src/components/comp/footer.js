import React, { Component } from 'react';

import '../../stylesheets/components/footer.css';

class Footer extends Component {

  render() {
    return (
        <div className="footer">
            <p> <span className= "blue">&copy; The Conduit Initative LLC.</span> All Rights Reserved.</p>
        </div>
    );
  }
}

export default Footer;
