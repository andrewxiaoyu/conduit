import React, { Component } from 'react';

import '../../stylesheets/components/initiative.css';

var $ = require("jquery");

const companies = [
    ["http://vivensity.com","vivensity.png"]
]

class Initiative extends Component {
    constructor(props){
        super(props);
        this.count = parseInt(Math.random()*companies.length);
        this.companyLink = companies[this.count][0];
        this.companyImg = require("../../img/"+companies[this.count][1]);
    }
    componentDidMount(){
        $(window).scroll(function() {
            var oS = 50;
            var wH = $(window).height(),
                wS = $(this).scrollTop();

            var h1 = $('#home1').offset().top,
                H1 = $('#home1').outerHeight();

            var h2 = $('#home2').offset().top,
                H2 = $('#home2').outerHeight();
                
            if ( (wS-oS) > (h1+H1-wH)){
                $('#home1').removeClass("photo");
                $('#home1').addClass("photoAnimated");
            }
            if ( (wS-oS) > (h2+H2-wH)){
                $('#home2').removeClass("photo");
                $('#home2').addClass("photoAnimated");
            }
        });
    }
    componentWillUnmount(){
        $(window).off();
    }
    render() {
        return (
            <div className="initiative">
                <div className="panel">
                    <h2>Trusted By</h2>
                    <a target="_blank" href={this.companyLink}>
                        <img className="company" src={this.companyImg} />
                    </a>
                    {/* <h2 className="company">{companies[this.count]}</h2> */}
                </div>
                <div className="panel">
                    <div className="info">
                        <h3>Websites. Apps. And Everything Else.</h3>
                        <p>We offer a variety of products, including splash/landing pages, fully-functional websites, and mobile applications.</p>
                    </div>
                    <img id="home1" className="photo" src={require("../../img/home1.png")} />
                </div>
                <div className="panel">
                    <div className="info">
                        <h3>Beautiful & Fast.</h3>
                        <p>Your app will be built rapidly by our development team. We'll make sure its perfect before deploying the final product.</p>
                    </div>
                    <img id="home2" className="photo" src={require("../../img/home2.png")}/>
                </div>
                <div className="panel">
                    <div className="option">
                        <h3>Order A Website</h3>
                        <button>Order A Website</button>
                    </div>
                    <div className="divider"></div>
                    <div className="option">
                        <h3>Join Us</h3>
                        <button>Work with Us</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Initiative;
