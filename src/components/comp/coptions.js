import React, { Component } from 'react';
import '../../stylesheets/components/contact.css';
import Order from './order';
import Join from './join';

class ContactOptions extends Component {
  render() {
    return (
      <div className="contact">
          <Join></Join>
          <div className="divider"></div>
          <Order></Order>
      </div>
    );
  }
}

export default ContactOptions;
