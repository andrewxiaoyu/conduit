import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../../stylesheets/components/pricingpanels.css';

class PricingPanels extends Component {
  render() {
    return (
        <div className="pricingpanelscont">
            <div className="pricingpanels">
                <div className="panel">
                    <div className="label">
                        <h2>Basic</h2>
                        <h3>$300</h3>
                    </div>
                    <p>Suitable for simple webpages or Andriod applications build with HTML/CSS and Java, respectively</p>
                    <ul>
                        <li>One Year Revision Period</li>
                        <li>2-3 Weeks Development</li>
                        <li>No SEO nor Google Analytics</li>
                    </ul>
                </div>
                <div className="panel">
                    <div className="label">
                        <h2>Professional</h2>
                        <h3>$600</h3>
                    </div>
                    <p>Best value for fully responsive website or mobile applications (either Andriod or iOS)</p>
                    <ul>
                        <li>Three Year Revision Period</li>
                        <li>4-6 Weeks Development</li>
                        <li>Basic Google Analytics</li>
                    </ul>
                </div>
                <div className="panel">
                    <div className="label">
                        <h2>Advanced</h2>
                        <h3>$900</h3>
                    </div>
                    <p>Full package deal for fully response website AND mobile application</p>
                    <ul>
                        <li>Limitless Revision Period</li>
                        <li>6+ Weeks Development</li>
                        <li>SEO and Google Analytics</li>
                    </ul>
                </div>
            </div>
            <div className="pageButton">
                <Link to="/contact"><button>Get A Quote</button></Link>
            </div>
        </div>
    );
  }
}

export default PricingPanels;
