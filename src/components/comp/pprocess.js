import React, { Component } from 'react';

import '../../stylesheets/components/initiative.css';

var $ = require("jquery");

class ProcessPanels extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
        $(window).scroll(function() {
            var oS = 50;
            var wH = $(window).height(),
                wS = $(this).scrollTop();

            var h1 = $('#dev').offset().top,
                H1 = $('#dev').outerHeight();

            var h2 = $('#pro').offset().top,
                H2 = $('#pro').outerHeight();

            var h3 = $('#rev').offset().top,
                H3 = $('#rev').outerHeight();
                
            if ( (wS-oS) > (h1+H1-wH)){
                $('#dev').removeClass("photo");
                $('#dev').addClass("photoAnimated");
            }
            if ( (wS-oS) > (h2+H2-wH)){
                $('#pro').removeClass("photo");
                $('#pro').addClass("photoAnimated");
            }
            if ( (wS-oS) > (h3+H3-wH)){
                $('#rev').removeClass("photo");
                $('#rev').addClass("photoAnimated");
            }
        });
    }
    componentWillUnmount(){
        $(window).off();
    }
    render() {
        return (
            <div className="initiative">
                <div className="panel">
                    <div className="info">
                        <h3>Consultation</h3>
                        <p>We'll discuss, delineate, and clarify your specific needs and draft a few proposals for the features in your target application.</p>
                    </div>
                    <img id="dev" className="photo" src={require("../../img/development.png")} />
                </div>
                <div className="panel">
                    <div className="info">
                        <h3>Design</h3>
                        <p>Multiple design concepts will be created and sent to you for approval. You'll also get a chance to meet the developers assigned to building your site.</p>
                    </div>
                    <img id="dev" className="photo" src={require("../../img/development.png")} />
                </div>
                <div className="panel">
                    <div className="info">
                        <h3>Development</h3>
                        <p>Our developers will build your app, with all their code viewable by you at all times. You'll also receive weekly email updates!</p>
                    </div>
                    <img id="pro" className="photo" src={require("../../img/process.png")} />
                </div>
                <div className="panel">
                    <div className="info">
                        <h3>Revision</h3>
                        <p>After you approve of the final site, we offer a complementary free one year term of small-scale edits (larger changes require a separate commission).</p>
                    </div>
                    <img id="rev" className="photo" src={require("../../img/revision.png")} />
                </div>
            </div>
        );
    }
}

export default ProcessPanels;
