import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Order extends Component {
  render() {
    return (
      <div className="order">
          <h2>Get A Quote</h2>
          <form acceptCharset="UTF-8" action="https://formkeep.com/f/526d5e2757e8" method="POST" id="order">
            <div className="inputcont">
              <h4>Name</h4>
              <input type="text" name="name" placeholder="John Smith" required/>
            </div>
            <div className="inputcont">
              <h4>Email</h4>
              <input type="email" name="email" placeholder="johnsmith@email.com" required/>
            </div>
            <div className="inputcont">
              <h4>Company</h4>
              <input type="text" name="company" placeholder="Company Name" required/>
            </div>
            <input type="submit"/>
          </form>
      </div>
    );
  }
}

export default Order;

/*
            <div className="inputcont">
              <h4>Description</h4>
              <textarea name="details" rows="5" cols="50" placeholder="Describe the details of your app." required>
              </textarea>
            </div>
            */